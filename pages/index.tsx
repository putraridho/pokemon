import {
  Box,
  BoxProps,
  Flex,
  Grid,
  IconButton,
  Spinner,
} from '@chakra-ui/react';
import axios from 'axios';
import { useQuery } from 'react-query';
import type { NextPage } from 'next';
import { useCallback, useMemo, useState } from 'react';

import { IPokemon, IPokemonResponse } from 'types/pokemon';
import { IPokemonQuery } from 'types/pokemon-query';

import PokeCard from 'components/PokeCard';
import Pagination from 'components/Pagination';
import PokeBigCard from 'components/PokeBigCard';
import { FavoriteIcon, FavoriteSolidIcon } from 'components/Icon';
import Favorite from 'components/Favorite';

import { useContextValue } from 'context';

const Home: NextPage = function () {
  const [pageSize] = useState<number>(12);
  const [page, setPage] = useState<number>(1);
  const [count, setCount] = useState<number>(0);
  const [endpoint] = useState<string>('https://pokeapi.co/api/v2/pokemon');
  const { favorites, selected, setFavorites, setSelected } = useContextValue();

  // fetch pokemons per-page
  const { data: state, isLoading } = useQuery<IPokemonQuery>(
    [`page ${page}`, endpoint],
    async () => {
      const res = await axios.get(endpoint, {
        params: {
          offset: (page - 1) * pageSize,
          limit: pageSize,
        },
      });
      return res.data;
    },
    {
      onSuccess: (data) => {
        setCount(data.count);
        if (!selected) setSelected(data.results[0].name);
      },
    },
  );

  const handleSelected = useCallback(
    (name: string) => {
      setSelected(name);
    },
    [setSelected],
  );

  return (
    <Grid position="relative" templateColumns="repeat(2, 1fr)" gap={8}>
      {/* Pokemon Grid List */}
      <Box maxH="100vh" overflow="auto" pl={8}>
        {!isLoading && !!state ? (
          <Grid templateColumns="repeat(2, 1fr)" gap={8} py={8}>
            {state?.results.map((pokeItem) => (
              <PokeItem
                key={pokeItem.name}
                pokemonResponse={pokeItem}
                onClick={() => handleSelected(pokeItem.name)}
              />
            ))}
          </Grid>
        ) : (
          <Flex py={8} justifyContent="center">
            <Spinner size="lg" thickness="6px" />
          </Flex>
        )}
        {/* Pagination inside Grid List */}
        <Flex py={4} justifyContent="center">
          <Pagination
            page={page}
            pageSize={pageSize}
            count={count}
            setPage={setPage}
            isLoading={isLoading}
          />
        </Flex>
      </Box>

      {/* Pokemon Big Detail */}
      <Box h="100vh" overflow="auto" pr={8}>
        <Flex py={8} minH="100%">
          {selected && <PokeDetail />}
        </Flex>
      </Box>

      {/* Pokemon favorite list */}
      <Box position="absolute" bottom={8} right={8}>
        <Favorite
          favorites={favorites}
          removeItem={(fav) => {
            setFavorites((curr) => curr.filter((item) => item.id !== fav.id));
          }}
        />
      </Box>
    </Grid>
  );
};

interface IPokeItem extends BoxProps {
  pokemonResponse: IPokemonResponse;
}

const PokeItem: React.FC<IPokeItem> = function ({ pokemonResponse, onClick }) {
  const { favorites, setFavorites } = useContextValue();

  // fetch pokemon per-item in grid
  const { data, isLoading } = useQuery<IPokemon>(
    ['fetch pokemon', pokemonResponse.name],
    async () => {
      const res = await axios.get(pokemonResponse.url);
      return res.data;
    },
  );

  const isFavorited = useMemo<boolean>(() => {
    if (data) {
      return favorites.some((fav) => fav.id === data.id);
    }

    return false;
  }, [data, favorites]);

  const handleFavorite = useCallback(
    (pokemon?: IPokemon) => {
      if (!pokemon) return;

      setFavorites((curr) => {
        const isFound = favorites.some((fav) => fav.id === pokemon.id);

        // if pokemon is already on favorite list, remove it
        if (isFound) {
          return curr.filter((fav) => fav.id !== pokemon.id);
        }

        // else add to favorite list
        return [...curr, pokemon].sort((a, b) => (a.id > b.id ? 1 : -1));
      });
    },
    [favorites, setFavorites],
  );

  return (
    <Box position="relative">
      {/* pokemon card */}
      <Box onClick={onClick}>
        <PokeCard pokemon={data} isLoading={isLoading} />
      </Box>
      {/* favorite button */}
      <Box position="absolute" bottom={3} right={3} zIndex={3}>
        <IconButton
          aria-label={
            isFavorited ? 'remove from favorites' : 'add to favorites'
          }
          icon={
            isFavorited ? (
              <FavoriteSolidIcon boxSize={6} fill="red.600" />
            ) : (
              <FavoriteIcon boxSize={6} />
            )
          }
          size="sm"
          isRound
          colorScheme="transparent"
          cursor="pointer"
          _focus={{
            outline: 0,
          }}
          onClick={() => {
            handleFavorite(data);
          }}
        />
      </Box>
    </Box>
  );
};

interface IPokeDetail {}

const PokeDetail: React.FC<IPokeDetail> = function () {
  const { selected } = useContextValue();

  // fetch the current selected Pokemon
  const { data, isLoading } = useQuery<IPokemon>(
    ['fetch pokemon', selected],
    async () => {
      const res = await axios.get(
        `https://pokeapi.co/api/v2/pokemon/${selected}`,
      );
      return res.data;
    },
  );

  return <PokeBigCard pokemon={data} isLoading={isLoading} />;
};

export default Home;
