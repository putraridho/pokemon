import {
  createContext,
  Dispatch,
  SetStateAction,
  useContext,
  useMemo,
  useState,
} from 'react';
import { IPokemon } from 'types/pokemon';

interface IPokeContext {
  favorites: IPokemon[];
  setFavorites: Dispatch<SetStateAction<IPokemon[]>>;
  selected: string | undefined;
  setSelected: Dispatch<SetStateAction<string | undefined>>;
}

const PokeContext = createContext<IPokeContext>({
  favorites: [],
  setFavorites: () => null,
  selected: undefined,
  setSelected: () => null,
});

export const Provider: React.FC<{
  children: React.ReactNode;
}> = function ({ children }) {
  const [favorites, setFavorites] = useState<IPokemon[]>([]);
  const [selected, setSelected] = useState<string | undefined>();

  const value = useMemo<IPokeContext>(
    () => ({
      favorites,
      selected,
      setFavorites,
      setSelected,
    }),
    [favorites, selected],
  );

  return <PokeContext.Provider value={value}>{children}</PokeContext.Provider>;
};

export const useContextValue = () => useContext(PokeContext);
