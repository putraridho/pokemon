export interface IAPIResource {
  url: string;
}

export interface INamedAPIResource extends IAPIResource {
  name: string;
}
