import { IPokemonResponse } from './pokemon';

export interface IPokemonQuery {
  count: number;
  next: string | null;
  previous: string | null;
  results: IPokemonResponse[];
}
