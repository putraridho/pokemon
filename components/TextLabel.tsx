import { Box, Text } from '@chakra-ui/react';

interface ITextLabel {
  label?: string;
  text: string;
}

const TextLabel: React.FC<ITextLabel> = function ({ label, text }) {
  return (
    <Box>
      {label && (
        <Text fontSize="sm" fontWeight="700" mb={0}>
          {label}
        </Text>
      )}
      <Text fontSize="sm">{text}</Text>
    </Box>
  );
};
export default TextLabel;
