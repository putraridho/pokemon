import { DeleteIcon } from '@chakra-ui/icons';
import {
  Box,
  Flex,
  IconButton,
  Image,
  ListItem,
  Text,
  Tooltip,
  UnorderedList,
} from '@chakra-ui/react';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { IPokemon } from 'types/pokemon';
import toCapitalise from 'utils/capitalize';
import { FavoriteIcon } from './Icon';

interface IFavorite {
  favorites: IPokemon[];
  removeItem: (_item: IPokemon) => void;
}

const Favorite: React.FC<IFavorite> = function ({ favorites, removeItem }) {
  const [isOpen, setIsOpen] = useState<boolean>(false);

  const hasFavorites = useMemo<boolean>(
    () => favorites.length > 0,
    [favorites.length],
  );

  const handleClick = useCallback(() => {
    if (hasFavorites) {
      setIsOpen((curr) => !curr);
    }
  }, [hasFavorites]);

  useEffect(() => {
    if (!hasFavorites) {
      setIsOpen(false);
    }
  }, [hasFavorites]);

  return (
    <Box
      position="relative"
      transformOrigin="center"
      transform={hasFavorites ? 'scale(1)' : 'scale(0)'}
      opacity={hasFavorites ? 1 : 0}
      transition="all .1s ease-in"
    >
      <Tooltip
        label={
          hasFavorites
            ? isOpen
              ? 'Hide favorite list'
              : 'Show favorite list'
            : 'You have no favorite Pokémon'
        }
        placement="left"
      >
        <IconButton
          aria-label="favorite list"
          isRound
          size="lg"
          icon={<FavoriteIcon boxSize={6} color="red.500" />}
          onClick={handleClick}
        />
      </Tooltip>
      <FavoriteCounter count={favorites.length} />
      {isOpen && hasFavorites && (
        <FavoriteList favorites={favorites} removeItem={removeItem} />
      )}
    </Box>
  );
};

interface IFavoriteCounter {
  count: number;
}

const FavoriteCounter: React.FC<IFavoriteCounter> = function ({ count }) {
  return (
    <Box
      position="absolute"
      top={0}
      right={0}
      h={5}
      w={5}
      background="red.500"
      textAlign="center"
      transform="translateY(-50%)"
      borderRadius="50px"
    >
      <Text lineHeight="20px" fontSize="xs" fontWeight="700" color="white">
        {count}
      </Text>
    </Box>
  );
};

interface IFavoriteList {
  favorites: IPokemon[];
  removeItem: (_item: IPokemon) => void;
}

const FavoriteList: React.FC<IFavoriteList> = function ({
  favorites,
  removeItem,
}) {
  return (
    <Box
      position="absolute"
      bottom="100%"
      right="100%"
      py={3}
      w="320px"
      bgColor="gray.200"
      borderRadius={24}
    >
      <UnorderedList
        listStyleType="none"
        ml={0}
        py={0}
        px={6}
        maxH="200px"
        overflow="auto"
      >
        {favorites.map((fav) => (
          <ListItem key={fav.name} py={1}>
            <FavoriteListItem
              pokemon={fav}
              icon={
                <IconButton
                  aria-label="remove from favorite list"
                  icon={<DeleteIcon boxSize={4} />}
                  size="xs"
                  colorScheme="transparent"
                  color="red.300"
                  ml="auto"
                  onClick={() => removeItem(fav)}
                />
              }
            />
          </ListItem>
        ))}
      </UnorderedList>
    </Box>
  );
};

interface IFavoriteListItem {
  pokemon: IPokemon;
  icon: React.ReactNode;
}

const FavoriteListItem: React.FC<IFavoriteListItem> = function ({
  pokemon,
  icon,
}) {
  return (
    <Flex alignItems="center">
      <Text fontSize="sm" fontWeight="600">
        #{`00${pokemon.id}`.substr(-3)}
      </Text>
      <Image
        src={pokemon.sprites.front_default}
        alt={`${pokemon.name} front default`}
        w={12}
        htmlHeight={24}
        htmlWidth={24}
        ml={3}
      />
      <Text fontSize="sm" fontWeight="600" ml={3}>
        {pokemon.name
          .split('-')
          .map((s) => toCapitalise(s))
          .join(' ')}
      </Text>
      {icon}
    </Flex>
  );
};

export default Favorite;
