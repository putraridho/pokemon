import {
  Box,
  Image,
  ListItem,
  Spinner,
  Text,
  UnorderedList,
} from '@chakra-ui/react';
import {
  IPokemon,
  IPokemonAbility,
  IPokemonMove,
  IPokemonSprites,
  IPokemonType,
} from 'types/pokemon';

import toCapitalise from 'utils/capitalize';
import { CardThemeType, ICardTheme } from 'utils/card-theme';
import useCardTheme from 'utils/useCardTheme';

import GridTemplate from './GridTemplate';
import TextLabel from './TextLabel';

const PokeBigCard: React.FC<IPokeBigCard> = function ({ pokemon, isLoading }) {
  const theme = useCardTheme(
    (pokemon?.types[0].type.name || 'normal') as CardThemeType,
  );

  return !isLoading && !!pokemon ? (
    <PokeBox theme={theme}>
      <PokeId id={pokemon.id} />
      <PokeName name={pokemon.name} />
      <UnorderedList display="flex" p={0} mb={2} ml={0} listStyleType="none">
        <PokeTypes types={pokemon.types} />
      </UnorderedList>
      <PokeSprite sprites={pokemon.sprites} />
      <PokeInfo
        height={pokemon.height}
        weight={pokemon.weight}
        base_experience={pokemon.base_experience}
      />
      <PokeAbilities abilities={pokemon.abilities} />
      <PokeMoves moves={pokemon.moves} />
    </PokeBox>
  ) : (
    <PokeBox theme={theme}>
      <Box
        position="absolute"
        top="50%"
        left="50%"
        transform="translate(-50%, -50%)"
      >
        <Spinner size="xl" thickness="6px" />
      </Box>
    </PokeBox>
  );
};

interface IPokeBox {
  children: React.ReactNode;
  theme: ICardTheme;
}

const PokeBox: React.FC<IPokeBox> = function ({ children, theme }) {
  return (
    <Box
      position="relative"
      borderRadius={32}
      bgGradient={theme.bgGradient}
      color={theme.color}
      py={6}
      px={8}
      boxShadow="0 8px 12px rgba(0, 0, 0, 0.16)"
      transition="all 0.2s linear"
      minH="100%"
      w="100%"
    >
      {children}
    </Box>
  );
};

interface IPokeSprite {
  sprites: IPokemonSprites;
}

const PokeSprite: React.FC<IPokeSprite> = function ({ sprites }) {
  return (
    <Image
      src={sprites.other['official-artwork'].front_default}
      alt="pokemon front default"
      htmlHeight={100}
      htmlWidth={100}
      w="45%"
      mx="auto"
      mb={4}
    />
  );
};

interface IPokeId {
  id: number;
}

const PokeId: React.FC<IPokeId> = function ({ id }) {
  return (
    <Text position="absolute" top={6} right={8} fontSize="2xl" fontWeight="700">
      #{`00${id}`.substr(-3)}
    </Text>
  );
};

interface IPokeName {
  name: string;
}

const PokeName: React.FC<IPokeName> = function ({ name }) {
  return (
    <Text as="h1" fontSize="2xl" fontWeight="700" mb={2}>
      {name
        .split('-')
        .map((s) => toCapitalise(s))
        .join(' ')}
    </Text>
  );
};

interface IPokeTypes {
  types: IPokemonType[];
}

const PokeTypes: React.FC<IPokeTypes> = function ({ types }) {
  return (
    <>
      {types.map(({ type }) => (
        <ListItem
          key={type.name}
          position="relative"
          mr={2}
          backgroundColor="rgba(255, 255, 255, .25)"
          borderRadius={25}
          lineHeight={1.785}
          fontSize="sm"
          fontWeight="medium"
          px={3}
          zIndex={1}
          _last={{
            mr: 0,
          }}
        >
          {toCapitalise(type.name)}
        </ListItem>
      ))}
    </>
  );
};

interface IPokeInfo {
  height: number;
  weight: number;
  base_experience: number;
}

const PokeInfo: React.FC<IPokeInfo> = function ({
  height,
  weight,
  base_experience,
}) {
  return (
    <Box mb={6}>
      <GridTemplate<{ label: string; value: string }>
        label="Info"
        array={[
          { label: 'Height', value: `${height / 10} m` },
          { label: 'Weight', value: `${weight / 10} kg` },
          { label: 'Base Exp.', value: `${base_experience}` },
        ]}
        render={({ label, value }) => (
          <TextLabel
            label={label}
            key={label}
            text={value
              .split('-')
              .map((s) => toCapitalise(s))
              .join(' ')}
          />
        )}
      />
    </Box>
  );
};

interface IPokeAbilities {
  abilities: IPokemonAbility[];
}

const PokeAbilities: React.FC<IPokeAbilities> = function ({ abilities }) {
  return (
    <Box mb={6}>
      <GridTemplate<IPokemonAbility>
        label="Abilities"
        array={abilities}
        render={({ ability }) => (
          <TextLabel
            key={ability.name}
            text={ability.name
              .split('-')
              .map((s) => toCapitalise(s))
              .join(' ')}
          />
        )}
      />
    </Box>
  );
};

interface IPokeMoves {
  moves: IPokemonMove[];
}

const PokeMoves: React.FC<IPokeMoves> = function ({ moves }) {
  return (
    <Box>
      <GridTemplate<IPokemonMove>
        label="Moves"
        array={moves}
        render={({ move }) => (
          <TextLabel
            key={move.name}
            text={move.name
              .split('-')
              .map((s: string) => toCapitalise(s))
              .join(' ')}
          />
        )}
      />
    </Box>
  );
};

interface IPokeBigCard {
  pokemon?: IPokemon;
  isLoading: boolean;
}

export default PokeBigCard;
