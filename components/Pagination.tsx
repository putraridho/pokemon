import {
  ArrowLeftIcon,
  ArrowRightIcon,
  ChevronLeftIcon,
  ChevronRightIcon,
} from '@chakra-ui/icons';
import { Box, Button, Flex, IconButton } from '@chakra-ui/react';
import { Dispatch, SetStateAction, useCallback, useMemo } from 'react';

interface IPagination {
  pageSize: number;
  page: number;
  count: number;
  setPage: Dispatch<SetStateAction<number>>;
  isLoading: boolean;
}

const Pagination: React.FC<IPagination> = function ({
  page,
  pageSize,
  count,
  setPage,
  isLoading,
}) {
  const totalPage = useMemo(
    () => Math.ceil(count / pageSize),
    [count, pageSize],
  );

  const handleFirstButton = useCallback(() => {
    setPage(1);
  }, [setPage]);

  const handlePrevButton = useCallback(() => {
    setPage((curr) => curr - 1);
  }, [setPage]);

  const handleNextButton = useCallback(() => {
    setPage((curr) => curr + 1);
  }, [setPage]);

  const handleLastButton = useCallback(() => {
    setPage(totalPage);
  }, [setPage, totalPage]);

  const handlePageButton = useCallback(
    (p) => {
      setPage(p);
    },
    [setPage],
  );

  const renderPage = useMemo<React.ReactNode>(() => {
    const window = 3;
    const halfWindow = Math.floor(window / 2);
    const lastCeil = Math.ceil(totalPage / window) - 1;

    return (
      <>
        {Array.from({ length: window }, (_, i) => {
          if (page - halfWindow < 1) {
            return i + 1;
          }

          if (page + halfWindow > totalPage) {
            return totalPage + i - halfWindow * 2;
          }

          return page + i - halfWindow;
        }).map((p) => (
          <Button
            key={p}
            type="button"
            size="sm"
            mr={2}
            _last={{ mr: 0 }}
            onClick={() => {
              if (page !== p) {
                handlePageButton(p);
              }
            }}
            pointerEvents={page === p ? 'none' : undefined}
            colorScheme={page === p ? 'blue' : 'gray'}
            disabled={isLoading}
          >
            {p}
          </Button>
        ))}
        {page + halfWindow < lastCeil && (
          <>
            <Box mr={2} px={4}>
              ...
            </Box>
            <Button
              type="button"
              size="sm"
              onClick={() => handlePageButton(totalPage)}
              disabled={isLoading}
            >
              {totalPage}
            </Button>
          </>
        )}
      </>
    );
  }, [handlePageButton, isLoading, page, totalPage]);

  return (
    <Flex alignItems="center">
      <Flex alignItems="center" mr={2}>
        <IconButton
          aria-label="first"
          size="sm"
          onClick={handleFirstButton}
          disabled={page <= 1 || isLoading}
          mr={2}
          icon={<ArrowLeftIcon boxSize={3} />}
        />
        <IconButton
          aria-label="prev"
          size="sm"
          onClick={handlePrevButton}
          disabled={page <= 1 || isLoading}
          icon={<ChevronLeftIcon boxSize={6} />}
        />
      </Flex>

      <Flex alignItems="flex-end">{renderPage}</Flex>

      <Flex alignItems="center" ml={2}>
        <IconButton
          aria-label="next"
          size="sm"
          onClick={handleNextButton}
          disabled={page >= totalPage || isLoading}
          mr={2}
          icon={<ChevronRightIcon boxSize={6} />}
        />
        <IconButton
          aria-label="last"
          size="sm"
          onClick={handleLastButton}
          disabled={page >= totalPage || isLoading}
          icon={<ArrowRightIcon boxSize={3} />}
        />
      </Flex>
    </Flex>
  );
};

export default Pagination;
