import { Box, Grid, Text } from '@chakra-ui/react';

interface IGridTemplate<T> {
  label: string;
  array: T[];
  render: (_item: T) => React.ReactNode;
}

const GridTemplate = function <T extends unknown>({
  label,
  array,
  render,
}: IGridTemplate<T>) {
  return (
    <Box>
      <Text fontSize="lg" fontWeight="700" mb={2}>
        {label}
      </Text>
      <Grid templateColumns="repeat(3, 1fr)" gap={2}>
        {array.map(render)}
      </Grid>
    </Box>
  );
};

export default GridTemplate;
