import {
  AspectRatio,
  Box,
  Image,
  ListItem,
  Spinner,
  Text,
  UnorderedList,
} from '@chakra-ui/react';

import { IPokemon, IPokemonSprites, IPokemonType } from 'types/pokemon';

import toCapitalise from 'utils/capitalize';
import { CardThemeType, ICardTheme } from 'utils/card-theme';
import useCardTheme from 'utils/useCardTheme';

interface IPokeCard {
  isLoading: boolean;
  pokemon?: IPokemon;
}

const PokeCard: React.FC<IPokeCard> = function ({
  isLoading = false,
  pokemon,
}) {
  const theme = useCardTheme(
    // set normal as the initial theme
    (pokemon?.types[0].type.name || 'normal') as CardThemeType,
  );

  return !isLoading && !!pokemon ? (
    <PokeBox theme={theme}>
      <Image
        src="/bg-pokeball.png"
        alt="pokeball"
        fallback={<></>}
        position="absolute"
        h={`${(223 / 249) * 100}%`}
        w="auto"
        bottom={0}
        left={19}
        transform={`translateY(${(83 / 223) * 100}%)`}
      />
      <Image
        src="/outline-vector.png"
        alt="outline vector"
        fallback={<></>}
        position="absolute"
        bottom="50%"
        right={8}
      />
      <PokeSprite sprites={pokemon.sprites} />
      <PokeId id={pokemon.id} />
      <PokeName name={pokemon.name} />
      <UnorderedList display="flex" p={0} mt={3} ml={0} listStyleType="none">
        <PokeTypes types={pokemon.types} />
      </UnorderedList>
    </PokeBox>
  ) : (
    <PokeBox theme={theme}>
      <Box
        position="absolute"
        top="50%"
        left="50%"
        transform="translate(-50%, -50%)"
      >
        <Spinner size="lg" thickness="6px" />
      </Box>
    </PokeBox>
  );
};

interface IPokeBox {
  children: React.ReactNode;
  theme: ICardTheme;
}

const PokeBox: React.FC<IPokeBox> = function ({ children, theme }) {
  return (
    <AspectRatio ratio={442 / 249}>
      <Box
        borderRadius={32}
        bgGradient={theme.bgGradient}
        color={theme.color}
        py={6}
        px={8}
        boxShadow="0 8px 12px rgba(0, 0, 0, 0.16)"
        transition="all 0.2s linear"
      >
        <Box h="100%" w="100%">
          {children}
        </Box>
      </Box>
    </AspectRatio>
  );
};
interface IPokeSprite {
  sprites: IPokemonSprites;
}

const PokeSprite: React.FC<IPokeSprite> = function ({ sprites }) {
  return (
    <Image
      position="absolute"
      src={sprites.front_default}
      alt="pokemon front default"
      fallback={<></>}
      top={0}
      right={0}
      h="100%"
      w="auto"
      zIndex={1}
    />
  );
};

interface IPokeId {
  id: number;
}

const PokeId: React.FC<IPokeId> = function ({ id }) {
  return (
    <Text
      position="absolute"
      top={6}
      right={8}
      zIndex={2}
      fontSize="md"
      fontWeight="500"
    >
      #{`00${id}`.substr(-3)}
    </Text>
  );
};

interface IPokeName {
  name: string;
}

const PokeName: React.FC<IPokeName> = function ({ name }) {
  return (
    <Text
      as="h1"
      position="relative"
      fontSize="2xl"
      fontWeight="700"
      lineHeight={1.167}
      zIndex={1}
    >
      {name
        .split('-')
        .map((s) => toCapitalise(s))
        .join(' ')}
    </Text>
  );
};

interface IPokeTypes {
  types: IPokemonType[];
}

const PokeTypes: React.FC<IPokeTypes> = function ({ types }) {
  return (
    <>
      {types.map(({ type }) => (
        <ListItem
          key={type.name}
          position="relative"
          mr={2}
          backgroundColor="rgba(255, 255, 255, .25)"
          borderRadius={25}
          lineHeight={1.785}
          fontSize="sm"
          fontWeight="medium"
          px={3}
          zIndex={1}
          _last={{
            mr: 0,
          }}
        >
          {toCapitalise(type.name)}
        </ListItem>
      ))}
    </>
  );
};

export default PokeCard;
