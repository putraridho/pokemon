export interface ICardTheme {
  bgGradient: string;
  color: string;
}

export type CardThemeType =
  | 'grass'
  | 'fire'
  | 'water'
  | 'bug'
  | 'dark'
  | 'dragon'
  | 'electric'
  | 'fairy'
  | 'fighting'
  | 'ice'
  | 'flying'
  | 'ghost'
  | 'ground'
  | 'poison'
  | 'psychic'
  | 'rock'
  | 'steel'
  | 'normal';
