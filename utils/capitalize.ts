function toCapitalise(str: string): string {
  return str[0].toUpperCase() + str.substring(1);
}

export default toCapitalise;
